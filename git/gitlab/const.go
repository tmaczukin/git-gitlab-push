package gitlab

const (
	pushOptionFlag  = "--push-option"
	setUpstreamFlag = "--set-upstream"
	forceFlag       = "--force-with-lease"

	ciOptionPrefix = "ci"
	mrOptionPrefix = "merge_request"

	ciOptionSkip     = "skip"
	ciOptionVariable = "variable"

	mrOptionCreate             = "create"
	mrOptionTarget             = "target"
	mrOptionTitle              = "title"
	mrOptionMWPS               = "merge_when_pipeline_succeeds"
	mrOptionRemoveSourceBranch = "remove_source_branch"
	mrOptionLabel              = "label"
	mrOptionUnlabel            = "unlabel"
)
