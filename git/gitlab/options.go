package gitlab

import (
	"gitlab.com/tmaczukin/git-gitlab-push/git"
)

type Options struct {
	Force        bool
	SetUpstream  bool
	PushRemote   string
	SourceBranch string

	CI CIOptions
	MR MROptions
}

type CIOptions struct {
	Skip      bool
	Variables []string
}

type MROptions struct {
	Create bool

	TargetBranch string
	Title        string

	Labels   []string
	Unlabels []string

	MergeWhenPipelineSucceeds bool
	RemoveSourceBranch        bool
}

func BuildArguments(options Options) git.Arguments {
	args := &argumentsBuilder{
		pushOptionFlag: pushOptionFlag,
		arguments:      make(git.Arguments, 0),
	}

	args.SetForce(options)
	args.SetCISkip(options.CI)
	args.AddCIVariables(options.CI)
	args.SetMRCreate(options.MR)
	args.SetMRTargetBranch(options.MR)
	args.SetMRTitle(options.MR)
	args.SetMRMWPS(options.MR)
	args.SetMRRemoveSourceBranch(options.MR)
	args.AddMRLabels(options.MR)
	args.AddMRUnlabels(options.MR)
	args.SetSetUpstream(options)
	args.AddArgument(options.PushRemote, "")
	args.AddArgument(options.SourceBranch, "")

	return args.Elements()
}
