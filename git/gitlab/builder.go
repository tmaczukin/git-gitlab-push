package gitlab

import (
	"fmt"

	"gitlab.com/tmaczukin/git-gitlab-push/git"
)

type argumentsBuilder struct {
	pushOptionFlag string
	arguments      git.Arguments
}

func (a *argumentsBuilder) Elements() git.Arguments {
	return a.arguments
}

func (a *argumentsBuilder) SetForce(o Options) {
	if !o.Force {
		return
	}

	a.AddArgument(forceFlag, "")
}

func (a *argumentsBuilder) AddArgument(name string, value string) {
	a.arguments = append(a.arguments, git.Argument{Name: name, Value: value})
}

func (a *argumentsBuilder) SetSetUpstream(o Options) {
	if !o.SetUpstream {
		return
	}

	a.AddArgument(setUpstreamFlag, "")
}

func (a *argumentsBuilder) SetCISkip(o CIOptions) {
	if !o.Skip {
		return
	}

	a.addPushOption(ciOption(ciOptionSkip))
}

func (a *argumentsBuilder) addPushOption(option string) {
	a.AddArgument(a.pushOptionFlag, option)
}

func (a *argumentsBuilder) AddCIVariables(o CIOptions) {
	for _, variable := range o.Variables {
		a.addPushOption(ciOptionWithValue(ciOptionVariable, variable))
	}
}

func (a *argumentsBuilder) SetMRCreate(o MROptions) {
	if !o.Create {
		return
	}

	a.addPushOption(mrOption(mrOptionCreate))
}

func (a *argumentsBuilder) SetMRTargetBranch(o MROptions) {
	if !o.Create || o.TargetBranch == "" {
		return
	}

	a.addPushOption(mrOptionWithValue(mrOptionTarget, o.TargetBranch))
}

func (a *argumentsBuilder) SetMRTitle(o MROptions) {
	if !o.Create || o.Title == "" {
		return
	}

	a.addPushOption(mrOptionWithValue(mrOptionTitle, o.Title))
}

func (a *argumentsBuilder) SetMRMWPS(o MROptions) {
	if !o.Create || !o.MergeWhenPipelineSucceeds {
		return
	}

	a.addPushOption(mrOption(mrOptionMWPS))
}

func (a *argumentsBuilder) SetMRRemoveSourceBranch(o MROptions) {
	if !o.Create || !o.RemoveSourceBranch {
		return
	}

	a.addPushOption(mrOption(mrOptionRemoveSourceBranch))
}

func (a *argumentsBuilder) AddMRLabels(o MROptions) {
	for _, label := range o.Labels {
		a.addPushOption(mrOptionWithValue(mrOptionLabel, label))
	}
}

func (a *argumentsBuilder) AddMRUnlabels(o MROptions) {
	for _, label := range o.Unlabels {
		a.addPushOption(mrOptionWithValue(mrOptionUnlabel, label))
	}
}

func ciOptionWithValue(opt string, value string) string {
	return optionWithValue(ciOption(opt), value)
}

func ciOption(opt string) string {
	return option(ciOptionPrefix, opt)
}

func mrOptionWithValue(opt string, value string) string {
	return optionWithValue(mrOption(opt), value)
}

func mrOption(opt string) string {
	return option(mrOptionPrefix, opt)
}

func optionWithValue(opt string, value string) string {
	return fmt.Sprintf("%s=%s", opt, value)
}

func option(prefix string, opt string) string {
	return fmt.Sprintf("%s.%s", prefix, opt)
}
