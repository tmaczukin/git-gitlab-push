package git

import (
	"fmt"
	"path/filepath"
	"strings"

	"github.com/go-git/go-billy/v5/osfs"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/cache"
	"github.com/go-git/go-git/v5/storage/filesystem"
)

type Remotes map[string]string

type Reader interface {
	CurrentBranchName() (string, error)
	Remotes() (Remotes, error)
	CurrentCommitTitle() (string, error)
	CurrentCommitMessage() (string, error)
}

func NewReader(workingDirectory string) (Reader, error) {
	wd := osfs.New(workingDirectory)
	gitDir := osfs.New(filepath.Join(workingDirectory, ".git"))

	storage := filesystem.NewStorage(gitDir, cache.NewObjectLRUDefault())

	r, err := git.Open(storage, wd)
	if err != nil {
		return nil, fmt.Errorf("opening git repository at %q: %w", workingDirectory, err)
	}

	g := &defaultReader{
		repo: r,
	}

	return g, nil
}

type defaultReader struct {
	repo *git.Repository
}

func (r *defaultReader) CurrentBranchName() (string, error) {
	head, err := r.repo.Head()
	if err != nil {
		return "", fmt.Errorf("requesting HEAD info: %w", err)
	}

	name := head.Name()
	if !name.IsBranch() {
		return "", fmt.Errorf("HEAD is not a branch")
	}

	return name.Short(), nil
}

func (r *defaultReader) Remotes() (Remotes, error) {
	remotes, err := r.repo.Remotes()
	if err != nil {
		return nil, fmt.Errorf("listing remotes: %w", err)
	}

	result := make(Remotes)

	for _, remote := range remotes {
		remoteConfig := remote.Config()

		if len(remoteConfig.URLs) < 1 {
			continue
		}

		result[remoteConfig.Name] = remoteConfig.URLs[0]
	}

	return result, nil
}

func (r *defaultReader) CurrentCommitTitle() (string, error) {
	message, err := r.CurrentCommitMessage()
	if err != nil {
		return "", nil
	}

	messageLines := strings.Split(message, "\n")

	return messageLines[0], nil
}

func (r *defaultReader) CurrentCommitMessage() (string, error) {
	head, err := r.repo.Head()
	if err != nil {
		return "", fmt.Errorf("requesting HEAD info: %w", err)
	}

	commit, err := r.repo.CommitObject(head.Hash())
	if err != nil {
		return "", fmt.Errorf("requesting current commit: %w", err)
	}

	return commit.Message, nil
}
