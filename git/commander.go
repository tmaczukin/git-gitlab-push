package git

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
)

const (
	command     = "git"
	pushCommand = "push"
)

type Arguments []Argument

type Argument struct {
	Name  string
	Value string
}

type Commander struct{}

func NewCommander() *Commander {
	return new(Commander)
}

func (c *Commander) DryRun(arguments Arguments) error {
	return c.Run(append([]Argument{{Name: "--dry-run"}}, arguments...))
}

func (c *Commander) Run(arguments []Argument) error {
	c.print(arguments)

	args := []string{pushCommand}
	for _, arg := range arguments {
		args = append(args, arg.Name)

		if arg.Value != "" {
			args = append(args, arg.Value)
		}
	}

	cmd := exec.Command(command, args...) // #nosec
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}

func (c *Commander) print(arguments []Argument) {
	cmd := []string{command + " " + pushCommand}
	for _, arg := range arguments {
		cmd = append(cmd, "\t"+strings.TrimSpace(fmt.Sprintf("%s %s", arg.Name, arg.Value)))
	}

	fmt.Println()
	fmt.Println("Running command:")
	fmt.Println()
	fmt.Println("   ", strings.Join(cmd, " \\\n"))
	fmt.Println()
}
