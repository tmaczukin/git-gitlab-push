package push

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/mitchellh/go-homedir"
	"github.com/urfave/cli"

	git_gitlab_mr "gitlab.com/tmaczukin/git-gitlab-push"
	"gitlab.com/tmaczukin/git-gitlab-push/config"
	"gitlab.com/tmaczukin/git-gitlab-push/git"
	"gitlab.com/tmaczukin/git-gitlab-push/git/gitlab"
	"gitlab.com/tmaczukin/git-gitlab-push/terminal"
)

const (
	defaultTarget         = "master"
	defaultRemote         = "origin"
	defaultConfigFileName = ".git-gitlab-push.yml"
)

var (
	homeDir string
)

func init() {
	var err error

	homeDir, err = homedir.Dir()
	if err != nil {
		panic(fmt.Sprintf("Error while searching home dir: %v", err))
	}
}

type GitLab struct {
	cfg  config.Config
	git  git.Reader
	term terminal.Terminal

	ConfigFile     string `long:"config-file" short:"c" description:"Path to the configuration file"`
	NoColor        bool   `long:"no-color" description:"Disable ANSI colors in output"`
	NonInteractive bool   `long:"non-interactive" short:"n" description:"Run in non-interactive mode"`
	DryRun         bool   `long:"dry-run" description:"Don't execute git push, just print the command"`

	Force       bool `long:"force" short:"f" description:"Do a force-push with '--force-with-lease' option"`
	SetUpstream bool `long:"set-upstream" short:"u" description:"Set the upstream of the pushed branch to chosen remote"`

	SourceBranch string `long:"source-branch" description:"Source branch to use"`
	Remote       string `long:"remote" description:"Remote to push the changes to"`

	CISkip     bool     `long:"ci.skip" description:"When used the push will not trigger a CI/CD pipeline"`
	CIVariable []string `long:"ci.variable" description:"Allows to pass custom variables to the started CI/CD pipeline. Variables are expected in format of 'KEY=value'. Use multiple times to apply multiple variables"`

	MRCreate             bool     `long:"mr.create" description:"When used the merge request creation will be requested"`
	MRTargetBranch       string   `long:"mr.target-branch" description:"Target branch to use for merge request creation. Works only with '--mr.create'"`
	MRLabelSets          []string `long:"mr.label-set" description:"The name of a predefined label set to use. Label sets can be defined within the configuration files. Use multiple times to apply multiple sets."`
	MRTitle              string   `long:"mr.title" description:"The title for the created merge request. Defaults to last commit subject if not specified. Works only with '--mr.create'"`
	MRMWPS               bool     `long:"mr.mwps" description:"When used the created merge request will be set to 'Merge when pipeline succeeds'. Works only with '--mr.create'"`
	MRRemoveSourceBranch bool     `long:"mr.remove-source-branch" description:"When used the merge request will be set to remove the source branch after merging. Works only with '--mr.create'"`
	MRUnlabels           []string `long:"mr.unlabel" description:"The name of label that should be removed from the merge request. Use multiple times to remove multiple labels"`

	mrLabels []string
	args     []string
}

func NewGitLab() *GitLab {
	return &GitLab{
		ConfigFile:           filepath.Join(homeDir, defaultConfigFileName),
		NoColor:              false,
		NonInteractive:       false,
		DryRun:               false,
		Force:                false,
		SetUpstream:          false,
		SourceBranch:         "",
		Remote:               defaultRemote,
		MRCreate:             false,
		MRTargetBranch:       defaultTarget,
		MRLabelSets:          make([]string, 0),
		MRTitle:              "",
		MRMWPS:               false,
		MRRemoveSourceBranch: false,
		MRUnlabels:           make([]string, 0),
		mrLabels:             make([]string, 0),
	}
}

func (p *GitLab) Execute(c *cli.Context) error {
	fmt.Println(git_gitlab_mr.Version().SimpleLine())
	fmt.Println("Pushing changes with GitLab integration")
	fmt.Println()

	p.args = c.Args()

	steps := []func() error{
		p.setupTerminal,
		p.loadConfiguration,
		p.prepareGitReader,
		p.getSourceBranch,
		p.getTargetBranch,
		p.getRemote,
		p.getLabels,
		p.execute,
	}

	for _, fn := range steps {
		err := fn()
		if err != nil {
			return err
		}
	}

	return nil
}

func (p *GitLab) setupTerminal() error {
	termFactory := terminal.NewColorTerminal
	if p.NoColor {
		termFactory = terminal.NewTerminal
	}

	p.term = termFactory()
	p.term.SetInteractiveMode(!p.NonInteractive)

	return nil
}

func (p *GitLab) loadConfiguration() error {
	configFilePath := p.ConfigFile
	if configFilePath == "" {
		return nil
	}

	cfg, err := config.LoadFromFile(configFilePath)
	if err != nil && !errors.Is(err, os.ErrNotExist) {
		return fmt.Errorf("loading configuration file: %v", err)
	}

	p.cfg = cfg

	return nil
}

func (p *GitLab) prepareGitReader() error {
	wd, err := os.Getwd()
	if err != nil {
		return fmt.Errorf("querying current working directory: %v", err)
	}

	r, err := git.NewReader(wd)
	if err != nil {
		return fmt.Errorf("initializing Git client: %v", err)
	}

	p.git = r

	return nil
}

func (p *GitLab) getSourceBranch() error {
	if p.SourceBranch == "" {
		var err error
		p.SourceBranch, err = p.git.CurrentBranchName()
		if err != nil {
			return fmt.Errorf("resolving current branch name: %w", err)
		}
	}

	sourceBranch, err := p.term.Ask("Source branch", p.SourceBranch)
	if err != nil {
		return fmt.Errorf("asking for source branch: %w", err)
	}

	p.SourceBranch = sourceBranch

	return nil
}

func (p *GitLab) getTargetBranch() error {
	targetBranch, err := p.term.Ask("Target branch", p.MRTargetBranch)
	if err != nil {
		return fmt.Errorf("asking for target branch: %w", err)
	}

	p.MRTargetBranch = targetBranch

	return nil
}

func (p *GitLab) getRemote() error {
	availableRemotes, err := p.git.Remotes()
	if err != nil {
		return fmt.Errorf("listing remotes: %w", err)
	}

	p.term.List("Available remotes", func(l terminal.ListEntryHandler) {
		for remote, URL := range availableRemotes {
			l(remote, URL)
		}
	})

	remote, err := p.term.Ask("Push to", p.Remote)
	if err != nil {
		return fmt.Errorf("asking for remote: %w", err)
	}

	p.Remote = remote

	return nil
}

func (p *GitLab) getLabels() error {
	labelsFromSets, err := p.getLabelsFromPredefinedSets()
	if err != nil {
		return err
	}

	p.mrLabels = append(p.mrLabels, labelsFromSets...)
	p.mrLabels = append(p.mrLabels, p.args...)

	return nil
}

func (p *GitLab) getLabelsFromPredefinedSets() ([]string, error) {
	sets := make(map[string][]string)

	p.term.List("Available label sets", func(l terminal.ListEntryHandler) {
		for _, labelSet := range p.cfg.LabelSets {
			l(labelSet.Name, strings.Join(labelSet.Labels, " "))
			sets[labelSet.Name] = labelSet.Labels
		}
	})

	defaultLabelSets := strings.Join(p.MRLabelSets, ",")
	chosenLabelSets, err := p.term.Ask("Comma separated list of label sets to apply", defaultLabelSets)
	if err != nil {
		return nil, fmt.Errorf("asking for label sets: %w", err)
	}

	labelSetNames := strings.Split(chosenLabelSets, ",")

	chosenLabels := make([]string, 0)
	for _, set := range labelSetNames {
		labels, ok := sets[strings.TrimSpace(set)]
		if !ok {
			continue
		}

		chosenLabels = append(chosenLabels, labels...)
	}

	return chosenLabels, nil
}

func (p *GitLab) execute() error {
	commander := git.NewCommander()

	args := gitlab.BuildArguments(gitlab.Options{
		Force:        p.Force,
		SetUpstream:  p.SetUpstream,
		PushRemote:   p.Remote,
		SourceBranch: p.SourceBranch,
		CI: gitlab.CIOptions{
			Skip:      p.CISkip,
			Variables: p.CIVariable,
		},
		MR: gitlab.MROptions{
			Create:                    p.MRCreate,
			MergeWhenPipelineSucceeds: p.MRMWPS,
			RemoveSourceBranch:        p.MRRemoveSourceBranch,
			TargetBranch:              p.MRTargetBranch,
			Title:                     p.MRTitle,
			Labels:                    p.mrLabels,
			Unlabels:                  p.MRUnlabels,
		},
	})

	var runErr error
	if p.DryRun {
		runErr = commander.DryRun(args)
	} else {
		runErr = commander.Run(args)
	}

	if runErr != nil {
		return fmt.Errorf("executing git command: %e", runErr)
	}

	return nil
}
