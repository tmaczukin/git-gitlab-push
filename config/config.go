package config

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	LabelSets LabelSets `yaml:"label_sets"`
}

type LabelSets []LabelSet

type LabelSet struct {
	Name   string   `yaml:"name"`
	Labels []string `yaml:"labels"`
}

func LoadFromFile(filePath string) (Config, error) {
	var cfg Config

	f, err := os.Open(filePath)
	if err != nil {
		return cfg, fmt.Errorf("opening file %q: %w", filePath, err)
	}

	defer f.Close() // #nosec

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&cfg)
	if err != nil {
		return cfg, fmt.Errorf("parsing YAML: %w", err)
	}

	return cfg, nil
}
