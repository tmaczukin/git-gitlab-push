package config

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v2"
)

var (
	TestFileContent = `
label_sets:
- name: set_1
  labels:
  - label_1
  - "label 2"
- name: "set 2"
  labels:
  - label_1
  - label 3
`

	TestConfig = Config{
		LabelSets: LabelSets{
			{
				Name: "set_1",
				Labels: []string{
					"label_1",
					"label 2",
				},
			},
			{
				Name: "set 2",
				Labels: []string{
					"label_1",
					"label 3",
				},
			},
		},
	}
)

func TestLoadFromFile(t *testing.T) {
	type fileCreatorFn func(t *testing.T) (string, func())

	createTestFile := func(content string) fileCreatorFn {
		return func(t *testing.T) (string, func()) {
			file, err := ioutil.TempFile("", "config-loading-test")
			require.NoError(t, err)

			_, err = fmt.Fprint(file, content)
			require.NoError(t, err)

			cleanup := func() {
				err := file.Close()
				require.NoError(t, err)

				err = os.RemoveAll(file.Name())
				require.NoError(t, err)
			}

			return file.Name(), cleanup
		}
	}

	tests := map[string]struct {
		prepareFile    fileCreatorFn
		expectedConfig Config
		assertError    func(t *testing.T, err error)
	}{
		"file opening error": {
			prepareFile: func(t *testing.T) (string, func()) {
				return "unknown-file", func() {}
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorIs(t, err, os.ErrNotExist)
			},
		},
		"parsing error": {
			prepareFile: createTestFile("invalid"),
			assertError: func(t *testing.T, err error) {
				var e *yaml.TypeError
				assert.ErrorAs(t, err, &e)
			},
		},
		"file properly loaded": {
			prepareFile:    createTestFile(TestFileContent),
			expectedConfig: TestConfig,
			assertError:    nil,
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			file, cleanup := tt.prepareFile(t)
			defer cleanup()

			config, err := LoadFromFile(file)

			if tt.assertError != nil {
				tt.assertError(t, err)
				return
			}

			assert.NoError(t, err)
			assert.Equal(t, tt.expectedConfig, config)
		})
	}
}
