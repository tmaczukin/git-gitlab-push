package terminal

import (
	"fmt"
	"os"
)

type Terminal interface {
	SetInteractiveMode(setTo bool)
	Panic(r interface{})
	Ask(message string, defaultValue string) (string, error)
	List(header string, handler func(c ListEntryHandler))
}

type defaultTerminal struct {
	interactive bool
	formatter   Formatter
}

func NewTerminal() Terminal {
	return &defaultTerminal{
		interactive: true,
		formatter:   NewDefaultFormatter(),
	}
}

func NewColorTerminal() Terminal {
	return &defaultTerminal{
		interactive: true,
		formatter:   NewColorFormatter(),
	}
}

func (t *defaultTerminal) SetInteractiveMode(setTo bool) {
	t.interactive = setTo
}

func (t *defaultTerminal) Panic(r interface{}) {
	fmt.Println(t.formatter.Panic(r))
	os.Exit(1)
}

func (t *defaultTerminal) Ask(message string, defaultValue string) (string, error) {
	prompt := NewPrompt(message, defaultValue)
	prompt.SetFormatter(t.formatter)

	prompt.Print()

	if !t.interactive {
		fmt.Println()

		return defaultValue, nil
	}

	return prompt.Ask()
}

func (t *defaultTerminal) List(header string, handler func(c ListEntryHandler)) {
	list := NewList(header)
	list.SetFormatter(t.formatter)
	list.Print(handler)
}
