package terminal

import (
	"fmt"
)

const (
	ANSIClear     = "\033[0m"
	ANSIRedBold   = "\033[31;1m"
	ANSIGreenBold = "\033[32;1m"
	ANSIWhiteBold = "\033[37;1m"
	ANSICyanBold  = "\033[36;1m"
)

type Formatter interface {
	Prompt(p *Prompt) string
	Panic(r interface{}) string
	ListHeader(header string) string
	ListEntry(id string, description string) string
}

type defaultFormatter struct{}

func NewDefaultFormatter() Formatter {
	return new(defaultFormatter)
}

func (f *defaultFormatter) Prompt(p *Prompt) string {
	prompt := p.message

	if p.defaultValue != "" {
		prompt += fmt.Sprintf(" [%s]", p.defaultValue)
	}

	return fmt.Sprintf("==> %s: ", prompt)
}

func (f *defaultFormatter) Panic(r interface{}) string {
	return fmt.Sprintf("Fatal error: %v", r)
}

func (f *defaultFormatter) ListHeader(header string) string {
	return fmt.Sprintf("%s:\n\n", header)
}

func (f *defaultFormatter) ListEntry(id string, description string) string {
	return fmt.Sprintf("%-20s \t %s\n", id, description)
}

type colorFormatter struct{}

func NewColorFormatter() Formatter {
	return new(colorFormatter)
}

func (f *colorFormatter) Prompt(p *Prompt) string {
	prompt := p.message

	if p.defaultValue != "" {
		prompt += fmt.Sprintf(" %s[%s]", ANSICyanBold, p.defaultValue)
	}

	return fmt.Sprintf("%s==> %s%s: %s", ANSIWhiteBold, ANSIGreenBold, prompt, ANSIClear)
}

func (f *colorFormatter) Panic(r interface{}) string {
	return fmt.Sprintf("%sFatal error: %s %v", ANSIRedBold, ANSIClear, r)
}

func (f *colorFormatter) ListHeader(header string) string {
	return fmt.Sprintf("%s%s:%s\n\n", ANSIRedBold, header, ANSIClear)
}

func (f *colorFormatter) ListEntry(id string, description string) string {
	return fmt.Sprintf("%s%-20s \t %s%s\n", ANSIWhiteBold, id, ANSIClear, description)
}
