package terminal

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type Prompt struct {
	message      string
	defaultValue string

	formatter Formatter
}

func NewPrompt(message string, defaultValue string) *Prompt {
	return &Prompt{
		message:      message,
		defaultValue: defaultValue,
	}
}

func (p *Prompt) SetFormatter(formatter Formatter) {
	p.formatter = formatter
}

func (p *Prompt) Print() {
	if p.formatter == nil {
		p.SetFormatter(NewDefaultFormatter())
	}
	promptLine := p.formatter.Prompt(p)

	fmt.Print(promptLine)
}

func (p *Prompt) Ask() (string, error) {
	reader := bufio.NewReader(os.Stdin)

	data, _, err := reader.ReadLine()
	if err != nil {
		return "", fmt.Errorf("reading input data: %v", err)
	}

	result := strings.TrimSpace(string(data))
	if result != "" {
		return result, nil
	}

	return p.defaultValue, nil
}
