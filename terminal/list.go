package terminal

import (
	"fmt"
)

type ListEntryHandler func(id string, description string)

type List struct {
	header string

	formatter Formatter
}

func NewList(header string) *List {
	return &List{
		header: header,
	}
}

func (l *List) SetFormatter(formatter Formatter) {
	l.formatter = formatter
}

func (l *List) Print(handler func(c ListEntryHandler)) {
	if l.formatter == nil {
		l.SetFormatter(NewDefaultFormatter())
	}

	fmt.Println()
	fmt.Println()
	fmt.Print(l.formatter.ListHeader(l.header))

	handler(l.printEntry)

	fmt.Println()
}

func (l *List) printEntry(id string, description string) {
	fmt.Print(l.formatter.ListEntry(id, description))
}
