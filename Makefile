export VERSION ?= $(shell (scripts/version 2>/dev/null || echo "dev") | sed -e 's/^v//g')
export REVISION ?= $(shell git rev-parse --short HEAD || echo "unknown")
export BRANCH ?= $(shell git show-ref | grep "$(REVISION)" | grep -v HEAD | awk '{print $$2}' | sed 's|refs/remotes/origin/||' | sed 's|refs/heads/||' | sort | head -n 1)
export BUILT ?= $(shell date -u +%Y-%m-%dT%H:%M:%S%z)
export CGO_ENABLED ?= 0
export GOPATH ?= ".go/"
export BUILD_PLATFORMS ?= -osarch 'linux/amd64' -osarch 'darwin/amd64'

export goJunitReport ?= $(GOPATH)/bin/go-junit-report
MOCKERY_VERSION = 1.1.0
MOCKERY = .tmp/mockery-$(MOCKERY_VERSION)
gox ?= $(GOPATH)/bin/gox

RELEASE_INDEX_GEN_VERSION ?= master
releaseIndexGen ?= .tmp/release-index-gen-$(RELEASE_INDEX_GEN_VERSION)
GITLAB_CHANGELOG_VERSION ?= master
gitlabChangelog = .tmp/gitlab-changelog-$(GITLAB_CHANGELOG_VERSION)
GO_RACE_REPORT_PARSER_VERSION ?= master
goRaceReportParser = .tmp/go-race-report-parser-$(GO_RACE_REPORT_PARSER_VERSION)

PKG := $(shell go list .)
PKGs := $(shell go list ./... | grep -vE "^/vendor/")

GO_LDFLAGS := -X $(PKG).VERSION=$(VERSION) \
              -X $(PKG).REVISION=$(REVISION) \
              -X $(PKG).BRANCH=$(BRANCH) \
              -X $(PKG).BUILT=$(BUILT) \
              -s -w

.PHONY: compile
compile:
	go build \
			-o build/git-gitlab-push \
			-ldflags "$(GO_LDFLAGS)" \
			./cmd/git-gitlab-push

.PHONY: compile_all
compile_all: $(gox)
	# Building project in version $(VERSION) for $(BUILD_PLATFORMS)
	$(gox) $(BUILD_PLATFORMS) \
			-ldflags "$(GO_LDFLAGS)" \
			-output="build/git-gitlab-push-{{.OS}}-{{.Arch}}" \
			./cmd/git-gitlab-push

export testsDir = ./.tests

.PHONY: tests
tests: $(testsDir) $(goJunitReport)
	@./scripts/tests normal

.PHONY: tests_race
tests_race: export CGO_ENABLED=1
tests_race: $(testsDir) $(goJunitReport)
	@./scripts/tests race

.PHONY: parse_race_report
parse_race_report: $(goRaceReportParser)
parse_race_report: $(testsDir)
	# Parsing race detector results
	@$(goRaceReportParser) parse --limit 0 $(testsDir)/output.txt

.PHONY: lint
lint: OUT_FORMAT ?= colored-line-number
lint: LINT_FLAGS ?=
lint:
	@golangci-lint run ./... --out-format $(OUT_FORMAT) $(LINT_FLAGS)
$(testsDir):
	# Preparing tests output directory
	@mkdir -p $@

.PHONY: fmt
fmt:
	# Fixing project code formatting...
	@go fmt $(PKGs) | awk '{if (NF > 0) {if (NR == 1) print "Please run go fmt for:"; print "- "$$1}} END {if (NF > 0) {if (NR > 0) exit 1}}'

.PHONY: mocks
mocks: $(MOCKERY)
	# Removing existing mocks
	@find * -name "mock_*.go" -delete
	# Generating new mocks
	@$(MOCKERY) -recursive -all -inpkg -dir ./

.PHONY: check_mocks
check_mocks:
	# Checking if mocks are up-to-date
	@$(MAKE) mocks
	# Checking the differences
	@git --no-pager diff --compact-summary --exit-code -- \
		$(shell git ls-files **/mock_*.go) \
		echo "Mocks up-to-date!"

.PHONY: check_modules
check_modules:
	@git diff go.sum > /tmp/gosum-$${CI_JOB_ID}-before
	@go mod tidy
	@git diff go.sum > /tmp/gosum-$${CI_JOB_ID}-after
	@diff -U0 /tmp/gosum-$${CI_JOB_ID}-before /tmp/gosum-$${CI_JOB_ID}-after

.PHONY: prepare_ci_image
prepare_ci_image: CI_IMAGE ?= git-gitlab-push
prepare_ci_image: CI_REGISTRY ?= ""
prepare_ci_image:
	# Building the $(CI_IMAGE) image
	@docker build \
			--pull \
			--no-cache \
			--build-arg GO_VERSION=$${GO_VERSION} \
			--build-arg ALPINE_VERSION=$${ALPINE_VERSION} \
			-t $(CI_IMAGE) \
			-f dockerfiles/ci/Dockerfile \
			dockerfiles/ci/
ifneq ($(CI_REGISTRY),)
	# Pushing the $(CI_IMAGE) image to $(CI_REGISTRY)
	@docker login --username $${CI_REGISTRY_USER} --password $${CI_REGISTRY_PASSWORD} $(CI_REGISTRY)
	@docker push $(CI_IMAGE)
	@docker logout $(CI_REGISTRY)
else
	# No CI_REGISTRY value, skipping image push
endif

latest_stable_tag := $(shell git -c versionsort.prereleaseSuffix="-rc" tag -l "v*.*.*" --sort=-v:refname | awk '!/rc/' | head -n 1)

.PHONY: release_s3
release_s3: CI_COMMIT_REF_NAME ?= $(BRANCH)
release_s3: CI_COMMIT_SHA ?= $(REVISION)
release_s3: S3_BUCKET ?=
release_s3:
	@$(MAKE) index_file
ifneq ($(S3_BUCKET),)
	@$(MAKE) sync_s3_release S3_URL="s3://$(S3_BUCKET)/$(CI_COMMIT_REF_NAME)/"
ifeq ($(shell git describe --exact-match --match $(latest_stable_tag) >/dev/null 2>&1; echo $$?), 0)
	@$(MAKE) sync_s3_release S3_URL="s3://$(S3_BUCKET)/latest/"
endif
	@$(MAKE) release_gitlab
endif

.PHONY: sync_s3_release
sync_s3_release: S3_URL ?=
sync_s3_release:
	# Syncing with $(GCS_URL)
	@aws --endpoint-url ${S3_ENDPOINT_URL} s3 sync build "$(S3_URL)" --acl public-read

.PHONY: remove_s3_release
remove_s3_release: CI_COMMIT_REF_NAME ?= $(BRANCH)
remove_s3_release: S3_BUCKET ?=
remove_s3_release:
ifneq ($(S3_BUCKET),)
	@aws --endpoint-url ${S3_ENDPOINT_URL} s3 rm "s3://$(S3_BUCKET)/$(CI_COMMIT_REF_NAME)" --recursive
endif

.PHONY: release_gitlab
release_gitlab: export CI_COMMIT_TAG ?=
release_gitlab: export CI_PROJECT_URL ?=
release_gitlab:
ifneq ($(CI_COMMIT_TAG),)
	# Saving as GitLab release at $(CI_PROJECT_URL)/-/releases
	@./scripts/gitlab_release
endif

.PHONY: index_file
index_file: export CI_COMMIT_REF_NAME ?= $(BRANCH)
index_file: export CI_COMMIT_SHA ?= $(REVISION)
index_file: $(releaseIndexGen)
	# generating index.html file
	@$(releaseIndexGen) \
		-working-directory build/ \
		-project-version $(VERSION) \
		-project-git-ref $(CI_COMMIT_REF_NAME) \
		-project-git-revision $(CI_COMMIT_SHA) \
		-project-name "GitLab Merge Request git integration" \
		-project-repo-url "https://gitlab.com/tmaczukin/git-gitlab-push"

.PHONY: generate_changelog
generate_changelog: export CHANGELOG_RELEASE ?= $(VERSION)
generate_changelog: $(gitlabChangelog)
	@$(gitlabChangelog) \
			-changelog-file CHANGELOG.md \
			-config-file .gitlab/changelog.yml \
			-project-id 21362395 \
			-release $(CHANGELOG_RELEASE) \
			-starting-point-matcher "v[0-9]*.[0-9]*.[0-9]*"

$(MOCKERY): OS_TYPE ?= $(shell uname -s)
$(MOCKERY): DOWNLOAD_URL = "https://github.com/vektra/mockery/releases/download/v$(MOCKERY_VERSION)/mockery_$(MOCKERY_VERSION)_$(OS_TYPE)_x86_64.tar.gz"
$(MOCKERY):
	# Installing $(DOWNLOAD_URL) as $(MOCKERY)
	@mkdir -p $(shell dirname $(MOCKERY))
	@curl -sL "$(DOWNLOAD_URL)" | tar xz -O mockery > $(MOCKERY)
	@chmod +x "$(MOCKERY)"

$(goJunitReport):
	# Installing $(goJunitReport)
	@go get github.com/jstemmer/go-junit-report

$(gox):
	# Installing $(gox)
	@go get github.com/mitchellh/gox

$(releaseIndexGen): OS_TYPE ?= $(shell uname -s | tr '[:upper:]' '[:lower:]')
$(releaseIndexGen): DOWNLOAD_URL = "https://storage.googleapis.com/gitlab-runner-tools/release-index-generator/$(RELEASE_INDEX_GEN_VERSION)/release-index-gen-$(OS_TYPE)-amd64"
$(releaseIndexGen):
	# Installing $(DOWNLOAD_URL) as $(releaseIndexGen)
	@mkdir -p $(shell dirname $(releaseIndexGen))
	@curl -sL "$(DOWNLOAD_URL)" -o "$(releaseIndexGen)"
	@chmod +x "$(releaseIndexGen)"

$(gitlabChangelog): OS_TYPE ?= $(shell uname -s | tr '[:upper:]' '[:lower:]')
$(gitlabChangelog): DOWNLOAD_URL = "https://storage.googleapis.com/gitlab-runner-tools/gitlab-changelog/$(GITLAB_CHANGELOG_VERSION)/gitlab-changelog-$(OS_TYPE)-amd64"
$(gitlabChangelog):
	# Installing $(DOWNLOAD_URL) as $(gitlabChangelog)
	@mkdir -p $(shell dirname $(gitlabChangelog))
	@curl -sL "$(DOWNLOAD_URL)" -o "$(gitlabChangelog)"
	@chmod +x "$(gitlabChangelog)"

$(goRaceReportParser): OS_TYPE ?= $(shell uname -s | tr '[:upper:]' '[:lower:]')
$(goRaceReportParser): DOWNLOAD_URL = "https://artifacts.maczukin.pl/go-race-report-parser/$(GO_RACE_REPORT_PARSER_VERSION)/go-race-report-parser-$(OS_TYPE)-amd64"
$(goRaceReportParser):
	# Installing $(DOWNLOAD_URL) as $(goRaceReportParser)
	@mkdir -p $(shell dirname $(goRaceReportParser))
	@curl -sL "$(DOWNLOAD_URL)" -o "$(goRaceReportParser)"
	@chmod +x "$(goRaceReportParser)"

print_ldflags:
	@echo $(GO_LDFLAGS)
