# GitLab integration command for Git push

When installed in `PATH` this application introduces a `git gitlab-push` command.

The command will ask few questions (or get the details with configuration options with non-interactive
execution mode) and finally do a `git push` that will automatically create a GitLab merge request
and/or adjust CI/CD pipeline execution.

In other words, it's a handy wrapper for https://docs.gitlab.com/ee/user/project/push_options.html.

## How it works

`git-gitlab-push` is called by Git when `git gitlab-push` command execution is attempted.

When executed it will gather data like:

- the source branch to use,
- the target branch that the merge request should point to,
- the remote to which the code should be pushed and where the merge request should be created,
- the set of labels to use (configurable outside of the binary; multiple sets allowed - labels will be concatenated).

Next it uses these details to build and execute a `git push` command with a set of push options described at
https://docs.gitlab.com/ee/user/project/push_options.html. With that the Git push will automatically create the
merge request and its URL will be printed in the command output.

## Installation

1. Go to https://gitlab.com/tmaczukin/git-gitlab-push/-/releases and find the latest version.

1. Download the binary for chosen os/architecture and save it locally. Rename the binary `git-gitlab-push`!

    ```shell
    cd /tmp
    wget -O ./git-gitlab-push https://artifacts.maczukin.pl/git-gitlab-push/v0.1.0/git-gitlab-push-linux-amd64
    ```

1. Make sure, that the file is executable.

    ```shell
    cd /tmp
    chmod +x ./git-gitlab-push
    ```

1. Make sure, that the binary is saved in the directory that is added to `PATH` variable. To check this, try
   to execute `git gitlab-push --version` in a random directory. If binary is accessible through `PATH` then
   it will work.

    ```shell
    cd /tmp
    sudo mv ./git-gitlab-push /usr/local/bin/git-gitlab-push
    ```

1. Try to use it with git:

    ```shell
    cd ~/
    git gitlab-push --version
    git gitlab-push help
    ```

## Usage

`git gitlab-push` by default works interactively. However, some details can be set upfront, with command line options.

Available data values are:

| Flag                        | Type      | Description |
|-----------------------------|-----------|-------------|
| `--force`, `-f`             | `boolean` | Do a force-push with `--force-with-lease` option. |
| `--set-upstream`, `-u`      | `boolean` | Set the upstream of the pushed branch to chosen remote. |
| `--source-branch`           | `string`  | Source branch to use. Defaults to current branch if not specified. |
| `--remote`                  | `string`  | Remote to push the changes to. Defaults to `origin` if not specified. |
| `--ci.skip`                 | `boolean` | When used the push will not trigger a CI/CD pipeline. |
| `--ci.variable`             | `strings` | Allows to pass custom variables to the started CI/CD pipeline. Variables are expected in format of `KEY=value`. Use multiple times to apply multiple variables. |
| `--mr.create`               | `boolean` | When used the merge request creation will be requested. |
| `--mr.target-branch`        | `string`  | Target branch to use for merge request creation. Defaults to `master` if not specified. **Works only with `--mr.create`**. |
| `--mr.label-set`            | `strings` | The name of a predefined label set to use. Label sets can be defined within the configuration files. Use multiple times to apply multiple sets.  |
| `--mr.title`                | `string`  | The title for the created merge request. Defaults to last commit subject if not specified. **Works only with `--mr.create`**. |
| `--mr.mwps`                 | `boolean` | When used the created merge request will be set to `Merge when pipeline succeeds`. **Works only with `--mr.create`**. |
| `--mr.remove-source-branch` | `boolean` | When used the merge request will be set to remove the source branch after merging. **Works only with `--mr.create`**. |
| `--mr.unlabel`              | `strings` | The name of label that should be removed from the merge request. Use multiple times to remove multiple labels. |

Additionally, the command will accept custom labels as arguments.

There are also some general options that configure `git-gitlab-push` behavior
and are not related with the Push operations:

| Flag                      | Type      | Description |
|---------------------------|-----------|-------------|
| `--config-file`, `-c`     | `string`  | Path to the configuration file. Defaults to `$HOME/.git-gitlab-push.yml` if not specified. |
| `--no-color`              | `boolean` | Disable ANSI colors in output. |
| `--non-interactive`, `-n` | `boolean` | Run in non-interactive mode. |
| `--dry-run `              | `boolean` | Don't execute git push, just print the command. |
| `-h`                      | `boolean` | Show help information. |
| `--version`, `-v`         | `boolean` | Print version information. |

The help information can be also printed with `git gitlab-push help` call.

### Example execution

<details>

```shell
$ git gitlab-push -u --mr.create
git-gitlab-push 0.3.0-beta-14-ge1cb24d (e1cb24d)
Pushing changes with GitLab integration

==> Source branch [update-documentation]:
==> Target branch [master]:


Available remotes:

origin                   git@gitlab.com:tmaczukin/git-gitlab-push.git

==> Push to [origin]:


Available label sets:

g_runner                 section::ops devops::verify group::runner Category:Runner
security                 security
documentation            documentation
pipelines_tooling        tooling tooling::pipelines
chef_backstage           backstage

==> Comma separated list of label sets to apply:

Running command:

    git push \
        --push-option merge_request.create \
        --push-option merge_request.target=master \
        --set-upstream \
        origin \
        update-documentation

Enumerating objects: 15, done.
Counting objects: 100% (15/15), done.
Delta compression using up to 8 threads
Compressing objects: 100% (6/6), done.
Writing objects: 100% (9/9), 5.86 KiB | 5.86 MiB/s, done.
Total 9 (delta 3), reused 0 (delta 0), pack-reused 0
remote:
remote: View merge request for update-documentation:
remote:   https://gitlab.com/tmaczukin/git-gitlab-push/-/merge_requests/7
remote:
To gitlab.com:tmaczukin/git-gitlab-push.git
 * [new branch]      update-documentation -> update-documentation
Branch 'update-documentation' set up to track remote branch 'update-documentation' from 'origin'.
```

</details>

In this example a `git gitlab-push -u --mr.create` command was executed. This started an interactive mode
of the application. Some details were provided using the default values (source branch name, target branch name,
remote). The labels applying was skipped as no set, nor a custom label in command arguments were provided.

After gathering all data, the application printed the details of `git push` command that is supposed to be executed:

<details>

```shell
git push \
    --push-option merge_request.create \
    --push-option merge_request.target=master \
    --set-upstream \
    origin \
    update-documentation
```

</details>

Finally, the `git push` command was executed, which ended with merge request creation.

## Configuration

In many cases there are sets of variables that are added to every merge request. To make life easier `git gitlab-push`
allows to configure such sets upfront. They can be next chosen during `git gitlab-push` execution. If multiple sets
are chosen, all variables will be concatenated (notice: with multiple scoped variables selected for applying this
may end with an unexpected result as only one of them should be applied by GitLab).

To use this feature `git gitlab-push` expects that it will be pointed to the configuration file with
the `--config-file` option. By , the command will assume that the file is named `.git-gitlab-push.yml` and
is present in user's home directory.

**Notice:** If the file pointed by `--config-file` option doesn't exist, the configuration loading is skipped
and `git gitlab-push` execution proceeds without the configuration details.

### Example configuration file

```yaml
label_sets:
- name: g_runner
  labels:
  - "section::ops"
  - "devops::verify"
  - "group::runner"
  - "Category:Runner"
- name: pipelines_tooling
  labels:
  - tooling
  - "tooling::pipelines"
```

This file defines two label sets:

- `g_runner` that defines four labels (`section::ops` and following),
- `pipelines_tooling` that defines two labels (`tooling` and following).

If `g_runner` will be chosen as the label set, the four labels from its definition will be added to the merge request.

If `security` will be chosen as the label set, then only the `security` label will be added to the merge request.

If `g_runner,security` or `security,g_runner` will be chosen as the label set, then all five labels will be added to the
merge request.

## Author

Tomasz Maczukin, 2020

## License

MIT
