module gitlab.com/tmaczukin/git-gitlab-push

go 1.15

require (
	github.com/go-git/go-billy/v5 v5.0.0
	github.com/go-git/go-git/v5 v5.1.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/stretchr/testify v1.6.2-0.20200720104044-95a9d909e987
	github.com/urfave/cli v1.22.5
	gitlab.com/ayufan/golang-cli-helpers v0.0.0-20171103152739-a7cf72d604cd
	gopkg.in/yaml.v2 v2.2.4
)
