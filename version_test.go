package git_gitlab_mr

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestVersionInfo_SimpleLine(t *testing.T) {
	assert.Equal(t, "git-gitlab-push dev (HEAD)", Version().SimpleLine())
}

func TestVersionInfo_Extended(t *testing.T) {
	v := Version().Extended()
	assert.Contains(t, v, "git-gitlab-push")
	assert.Contains(t, v, "Version:      dev")
	assert.Contains(t, v, "Git revision: HEAD")
	assert.Contains(t, v, "Git branch:   HEAD")
	assert.Contains(t, v, "GO version:   ")
	assert.Contains(t, v, "Built:        ")
	assert.Contains(t, v, "OS/Arch:      ")
}
