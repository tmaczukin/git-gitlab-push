package main

import (
	"fmt"
	"os"

	"github.com/urfave/cli"
	clihelpers "gitlab.com/ayufan/golang-cli-helpers"

	git_gitlab_mr "gitlab.com/tmaczukin/git-gitlab-push"
	"gitlab.com/tmaczukin/git-gitlab-push/git/push"
)

func main() {
	app := newApp()
	err := app.Run(os.Args)
	if err != nil {
		panic(fmt.Sprintf("Error: %v", err))
	}
}

func newApp() *cli.App {
	app := cli.NewApp()
	app.Name = git_gitlab_mr.NAME
	app.Version = git_gitlab_mr.Version().SimpleLine()
	app.Usage = "A git push <-> GitLab integration tool"
	app.UsageText = "git gitlab-push [global options] [custom_label_1 [custom_label_2 [...]]]"
	app.ArgsUsage = "[custom_label_1 [custom_label_2 [...]]]"
	app.Description = "Custom labels for removal arguments"

	glPush := push.NewGitLab()
	app.Flags = clihelpers.GetFlagsFromStruct(glPush)
	app.Action = glPush.Execute

	cli.HelpFlag = cli.BoolFlag{
		Name:  "h",
		Usage: "Show help information",
	}
	cli.VersionFlag = cli.BoolFlag{
		Name:  "version, v",
		Usage: "Print version information",
	}
	cli.VersionPrinter = func(c *cli.Context) {
		fmt.Println(git_gitlab_mr.Version().Extended())
	}

	return app
}
